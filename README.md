# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

 一.畫布區域為網頁右邊區塊

 二.左邊為工具列

 三.功能如下(由上而下):

    a.改變顏色(改片後刷毛顏色會變)
    b.筆刷大小

    c.改變文字的字體
    d.改變文字的大小

    e.畫筆
    f.擦子
    g.插入文字

    h.畫三角形
    i.畫圓形
    j.畫正方形
    (形狀btn重複點按可以選擇空心或實心)

    k.undo
    l.redo
    m.reset

    n.上傳圖片
    o.下載畫布
四.js:

    1.init() : 初始化畫布
    2.$(document).ready(function(){}) : 監聽大部分btn按下事件 以及顏色改變事件 以及字體大小字型
    3.drawText() : 畫文字
    4.Upload() : 上傳圖片

    5.md() : 按照目前狀態處理 mousedown
    6.mv() : 按照目前狀態處理 mousemove
    7.mup() : 按照目前狀態處理 mouseup
    狀態共有 畫三種形狀.畫畫.插入文字 等等

    8.changethickness() : 改變筆刷粗細
    9.cPush() cUndo() cRedo() : 將每一次筆畫or文字or形狀 用cPush 存到array裡面 運用cUndo以及cRedo
        來new image 並畫到畫布上
五.css:

    本次作業的css、圖片都是自己用電腦繪圖而成
