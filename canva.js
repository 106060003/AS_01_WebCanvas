var draw;
var canvas = document.getElementById("m");
var ctx ;
var btn_color = document.getElementById('btn_color');
var color = document.getElementById('color');
var btn_save = document.getElementById('btn_save');
var btn_eraser = document.getElementById('btn_eraser');
var btn_pen = document.getElementById('btn_pen');
var btn_reset = document.getElementById('btn_reset');
var btn_undo = document.getElementById('btn_undo');
var btn_redo = document.getElementById('btn_redo');
var btn_circle = document.getElementById('btn_circle');
var btn_rectangle = document.getElementById('btn_rectangle');
var btn_triangle = document.getElementById('btn_triangle');
var btn_upload = document.getElementById('btn_upload');
var btn_text = document.getElementById('btn_text');
var text_fontType = document.getElementById('text_fontType');
var text_fontSize = document.getElementById('text_fontSize');
var myText = document.getElementById('myText');


var cPushArray = new Array();
var cStep = -1;

var isDrawing = true;
var isDrawingRectangle = false;
var isDrawingTriangle = false;
var isDrawingCricle = false;
var isTexting = false;

var isFillCircle = true;
var isFillRectangle = true;
var isFillTriangle = true;


var shape_beg_x;
var shape_beg_y;
var shape_end_x;
var shape_end_y;
var a;
var b;
var r;
var x;
var y;
var ratioX;
var ratioY;
var temp = new Image();
temp.src = canvas.toDataURL();
var text_x = 100;
var text_y = 100;


function init(){
    ctx = canvas.getContext("2d");
    ctx.fillStyle = 'rgb(253, 249, 211)';
    ctx.lineJoin = "round";
    ctx.lineCap = "round";
    ctx.fillRect (0, 0, 960, 600);
    cPushArray = new Array();
    cStep = -1;
    ctx.font = "24px Arial";
    ctx.fillStyle = 'rgb(0, 0, 0)';
    text_fontType.value = "Arial";
    text_fontSize.value =  "24";
    cPush();
  }

document.getElementById('btn_color').onclick = function(){
    document.getElementById('color').click();
};

$(document).ready(function(){
    $(color).change(function(){
        $(btn_color).css({"background-color": color.value});
        ctx.strokeStyle = color.value;
        ctx.fillStyle = color.value;
      });
    $(btn_save).click(function(){
        $('#autoclickme a').get(0).click();
    })
    $(btn_upload).click(function(){
        $('#uploadImage').click();
        $("#uploadImage").change(function(){
            Upload();
          });
    })
    $(btn_eraser).click(function(){
        $(canvas).css({"cursor" : "url('erasericon.png'),auto" });
        ctx.strokeStyle = "#FDF9D3";
        ctx.lineJoin = "round";
        isDrawing = true;
        isDrawingRectangle = false;
        isDrawingTriangle = false;
        isDrawingCricle = false;
        isTexting = false;
      });
    $(btn_pen).click(function(){
        $(canvas).css({"cursor" : "url('penicon.png'),auto" });
        ctx.lineJoin = "round";
        ctx.strokeStyle = color.value ;
        ctx.fillStyle = color.value;
        isDrawing = true;
        isDrawingRectangle = false;
        isDrawingTriangle = false;
        isDrawingCricle = false;
        isTexting = false;
    })
    $(btn_reset).click(function(){
        init();
    })
    $(btn_undo).click(function(){
        cUndo();
    })
    $(btn_redo).click(function(){
        cRedo();
    })
    $(btn_text).click(function(){
        $(canvas).css({"cursor" : "text" });
        isDrawing = false;
        isDrawingRectangle = false;
        isDrawingTriangle = false;
        isDrawingCricle = false;
        isTexting = true;
    })
    $(btn_circle).click(function(){
        $(canvas).css({"cursor" : "crosshair" });
        ctx.strokeStyle = color.value ;
        ctx.fillStyle = color.value;
        isFillCircle = !isFillCircle;
        if(isFillCircle){
            $(btn_circle).css({"background-image": "url(circle.png)"});
        }
        else{
            $(btn_circle).css({"background-image": "url(circle_un.png)"});
        }
        isDrawing = false;
        isDrawingRectangle = false;
        isDrawingTriangle = false;
        isDrawingCricle = true;
        isTexting = false;
    })
    $(btn_triangle).click(function(){
        $(canvas).css({"cursor" : "crosshair" });
        ctx.lineJoin = "miter";
        ctx.strokeStyle = color.value ;
        ctx.fillStyle = color.value;
        isFillTriangle = !isFillTriangle;
        if(isFillTriangle){
            $(btn_triangle).css({"background-image": "url(triangle.png)"});
        }
        else{
            $(btn_triangle).css({"background-image": "url(triangle_un.png)"});
        }
        isDrawing = false;
        isDrawingRectangle = false;
        isDrawingTriangle = true;
        isDrawingCricle = false;
        isTexting = false;
    })
    $(btn_rectangle).click(function(){
        $(canvas).css({"cursor" : "crosshair" });
        ctx.lineJoin = "miter";
        ctx.strokeStyle = color.value ;
        ctx.fillStyle = color.value;
        isFillRectangle = !isFillRectangle;
        if(isFillRectangle){
            $(btn_rectangle).css({"background-image": "url(rectangle.png)"});
        }
        else{
            $(btn_rectangle).css({"background-image": "url(rectangle_un.png)"});
        }
        isDrawing = false;
        isDrawingRectangle = true;
        isDrawingTriangle = false;
        isDrawingCricle = false;
        isTexting = false;
    })
    $(text_fontType).change(function(){
        ctx.font = text_fontSize.value + "px " + text_fontType.value;
    })
    $(text_fontSize).change(function(){
        ctx.font = text_fontSize.value + "px " + text_fontType.value;
    })
  });

var img = new Image();
var input_text = new Text();

function drawText(){
    var text = $('.input').val().split('\n');
    if(text != ""){
        for(var i=0; i<text.length; i++) {
            ctx.fillText(text[i], text_x, text_y + parseInt(text_fontSize.value)*(i+1) - 14);
        }
        cPush();
    }
    draw = false;
    $('.input').remove();
}

function Upload() {
    var file = $('#uploadImage')[0].files[0];
    img.onload = function () {
        var a = img.width / img.height;
        var img_width ;
        var img_height ;
        if(a > 960/600){
            img_width = 960;
            img_height = img.height * 960 / img.width;
        }
        else{
            img_width = img.width * 600 / img.height;
            img_height = 600;
        }
        ctx.drawImage(img, 0, 0, img_width, img_height);
        cPush();
    }
    img.src = URL.createObjectURL(file);
}

function md(){
    if(isDrawing){
        ctx.moveTo(event.offsetX,event.offsetY);
        draw = true;
        ctx.beginPath();
    }
    else if(isDrawingCricle){
        temp.src = canvas.toDataURL();
        shape_beg_x = event.offsetX;
        shape_beg_y = event.offsetY;
        ctx.moveTo(event.offsetX,event.offsetY);
        draw = true;
    }
    else if(isDrawingRectangle){
        temp.src = canvas.toDataURL();
        shape_beg_x = event.offsetX;
        shape_beg_y = event.offsetY;
        ctx.moveTo(event.offsetX,event.offsetY);
        draw = true;
    }
    else if(isDrawingTriangle){
        temp.src = canvas.toDataURL();
        shape_beg_x = event.offsetX;
        shape_beg_y = event.offsetY;
        draw = true;
    }
    else if(isTexting){
        if(!draw){
            text_x = event.offsetX;
            text_y = event.offsetY;
        }
        temp.src = canvas.toDataURL();
        draw = true;
        input = $('<textarea>', {class:'input',onfocusout:'drawText()'}).css({
            'font' : ctx.font,
            'color': ctx.fillStyle,
            'position': 'absolute',
            'top': text_y -10 +'px',
            'left': text_x-4 +'px',
            'width': '300px',
            'height': text_fontSize.value*5 +'px',
            'background-color' : 'transparent'
        })
        input.focus();
        $('#canva').append(input)
    }
}
function mv(){
    if(isDrawing){
        if(draw){
            ctx.lineTo(event.offsetX,event.offsetY);
            ctx.stroke();
        }
    }
    else if(isDrawingCricle){
        if(draw){
            ctx.globalCompositeOperation = "copy";
            ctx.drawImage(temp, 0, 0);
            ctx.globalCompositeOperation = "source-over";
            shape_end_x = event.offsetX;
            shape_end_y = event.offsetY;
            a = ((shape_end_x - shape_beg_x) /2 > 0) ? (shape_end_x - shape_beg_x) /2:(shape_beg_x - shape_end_x) /2;
            b = ((shape_end_y - shape_beg_y) /2 > 0) ? (shape_end_y - shape_beg_y) /2:(shape_beg_y - shape_end_y) /2;
            
            x = (shape_end_x + shape_beg_x) / 2;
            y = (shape_end_y + shape_beg_y) / 2;
            r = (a > b) ? a : b; 
            ratioX = a / r;
            ratioY = b / r;
            ctx.save();
            ctx.scale(ratioX, ratioY);
            ctx.beginPath();
            ctx.arc(x/ratioX, y/ratioY, r, 0, 360,false);
            ctx.closePath();
            ctx.stroke();
            if(isFillCircle){
                ctx.fill();
            }
            ctx.restore();
        }
    }
    else if(isDrawingRectangle){
        if(draw){
            ctx.globalCompositeOperation = "copy";
            ctx.drawImage(temp, 0, 0);
            ctx.globalCompositeOperation = "source-over";
            shape_end_x = event.offsetX;
            shape_end_y = event.offsetY;
            a = (shape_end_x - shape_beg_x);
            b = (shape_end_y - shape_beg_y);

            ctx.beginPath();
            ctx.rect(shape_beg_x,shape_beg_y,a,b);
            ctx.stroke();
            if(isFillRectangle){
                ctx.fill();
            }
        }
    }
    else if(isDrawingTriangle){
        if(draw){
            ctx.globalCompositeOperation = "copy";
            ctx.drawImage(temp, 0, 0);
            ctx.globalCompositeOperation = "source-over";
            shape_end_x = event.offsetX;
            shape_end_y = event.offsetY;
            a = (shape_end_x - shape_beg_x);
            b = (shape_end_y - shape_beg_y);

            x = (shape_end_x + shape_beg_x) / 2;
            ctx.beginPath();
      
            ctx.moveTo(shape_beg_x, shape_end_y);
            ctx.lineTo(shape_end_x, shape_end_y);
            ctx.lineTo(x, shape_beg_y);
            ctx.lineTo(shape_beg_x, shape_end_y);
            ctx.closePath();
            ctx.stroke();
            if(isFillTriangle){
                ctx.fill();
            }
        }
    }
}
function mup(){
    if(isDrawing){
        cPush();
        draw = false;
    }
    else if(isDrawingCricle){
        draw = false;
        temp.src = canvas.toDataURL();
        cPush();
    }
    else if(isDrawingRectangle){
        draw = false;
        temp.src = canvas.toDataURL();
        cPush();
    }
    else if(isDrawingTriangle){
        draw = false;
        temp.src = canvas.toDataURL();
        cPush();
    }
    else if(isTexting){
        input.focus();
    }
}


function changethickness(){
    thickness = document.getElementById("thickness");
    ctx.lineWidth = thickness.value;
}
	
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('m').toDataURL());
}

function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}